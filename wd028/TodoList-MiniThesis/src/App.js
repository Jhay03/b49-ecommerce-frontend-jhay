import React, {useState, useEffect} from 'react';
import TodoItems from './components/TodoItems';
import AddTodo from './components/forms/AddTodo'; 
import { v4 as uuidv4 } from 'uuid';
import Header from './components/layouts/Header';


function App() {
  const [todos, setTodos] = useState([]);
  const [loading, setLoading] = useState(true);
  const [showForm, setShowForm] = useState(false);
{ /* const state = {
    todos: []
  }
*/}
  useEffect( () => {
    fetch("https://jsonplaceholder.typicode.com/todos")
      .then(res => res.json())
      .then(data => {
        setLoading(false)
        setTodos(data.slice(0, 5)) //limit the value of fetch to 2
      })
  }, [])
  //console.log(todos);
  

  //Adding Todo
  const addTodo = (formData) => {
    //alert(formData)
    // console.log(formData)
    setTodos([
      ...todos,
      {
        userId: 1,
        id: uuidv4(),
        title: formData.title
      }
    ])
  }

  //Editing new todo!
  const editTodo = ({ id, title }) => {
    const updatedTodos = todos.map(todo => {
      return todo.id === id ? { ...todo, title } : todo
    })
    setTodos(updatedTodos)
  }
  
  //deleting todo
  const deleteTodo = (todoId) => {
    let result = window.confirm("Are you sure want to delete this Todo??")
    if(result){
    const updatedTodos = todos.filter(todo => todo.id !== todoId)
      setTodos(updatedTodos)
    }
  }

  {/*
    const markComplete = (id) => {
      this.setState({
        todos: this.state.todos.map(todo => {
          if (tod.id === id) {
            todo.completed = !todo.completed
          }
          return todo;
        })
      })
    }*/
  }
  

  //style for add todo button
  const addBtn = {
    alignConten: "center",
    fontStlye: "none",
    fontSize: "10",
    marginLeft: "94%",
    marginBottom: "10px",
    marginTop: "10px"
  }

  const allTodos = todos.length ?
    todos.map(todo => (
      <TodoItems
        key={todo.id}
        todo={todo}
        editTodo={editTodo}
        deleteTodo={deleteTodo}
      />
    )).reverse()
    : <h1> No Todos</h1>
  
  
  const isLoading = loading ? /*<img src="https://i.gifer.com/1zes.gif" alt="gift" style={loadingStyle}/>*/
    <h1>Loading . . .</h1> : allTodos
  return (
    <div className="App">
      <Header/>
      {showForm ? <AddTodo
      addTodo = {addTodo}
      /> : 
        <button style={addBtn}
          onClick={() => setShowForm(true)}>
          Add Todo
          </button>}
          
      {isLoading}
    </div>
  );
}

export default App;
