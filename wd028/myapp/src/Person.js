import React, {Fragment} from 'react';


const Person = ({name, age}) => {
    return (
        <Fragment>
            <p> Hi I'm {name} and I am {age} years old.</p>
            </Fragment>
    )
}

export default Person;