import React, {useState, useEffect} from 'react';
import { useParams } from 'react-router-dom';
import Swal from 'sweetalert2';
import EditProduct from './forms/EditProduct';
import {URL} from "../config"
const Product = ({user,token}) => {
    let { id } = useParams()
    const [product, setProduct] = useState({})
    const [showEdit, setShowEdit] = useState(false)
    useEffect(() => {
        fetch(`${URL}/products/${id}`)
            .then(res => res.json())
        .then(data => setProduct(data))
    }, [])
    const style = {
        "height": "50vh",
        "width": "100%"
    }

    const deleteHandler = () => {
        Swal.fire({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
          if (result.value) {
            fetch(`${URL}/products/${id}`, {
                method: "DELETE",
                headers: {
                    "x-auth-token": token
                }
            })
            .then(res => res.json())
            .then(data => {
                Swal.fire(
                  'Deleted!',
                  `${data.message}`,
                  'success'
                )
            })
            
          }
        })
            Swal.fire({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#d33',
          cancelButtonColor: '#3085d6',
          confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
          if (result.value) {
            fetch(`${URL}/products/}${id}`, {
                method: "DELETE",
                headers: {
                    "x-auth-token": token
                }
            })
            .then(res => res.json())
            .then(data => {
                Swal.fire(
                  'Deleted!',
                  `${data.message}`,
                  'success'
                )
                window.location.href="/"
            })
            
          }
        })
    }
    return (
        <div className="container">
            <h2 className="text-center"> Product Details</h2>
            <div className="row col-sm-6">

                <div className="card">
                    {showEdit ? <EditProduct setShowEdit={setShowEdit} product={product} /> :
                    <div>
                        <img src={`${URL}/${product.image}`} style={style} />
                        <div className="card-title">    
                                <h5 className="text-center">Name: {product.name}</h5>
                            </div>
                        <div className="card-body">
                                <p>Description: {product.description}</p>
                                <h6>Price: {product.price}</h6>
                        </div>        
                                {user && user.isAdmin && token ?
                                        <div className="card-footer">
                                            <button className="btn btn-outline-warning mr-2" onClick={() => setShowEdit(true)}>Edit</button>
                                            <button className="btn btn-outline-danger"  onClick={deleteHandler}>Delete</button>
                                        </div> :
                                    null}
                    </div>}
                    
                </div>
            </div>
            </div>
    )
}
export default Product;