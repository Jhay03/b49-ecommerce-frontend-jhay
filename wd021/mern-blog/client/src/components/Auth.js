import React from 'react';
import { Link } from 'react-router-dom';

//dump component
const Auth = () => {
    return (
        <div className="container col-sm-6 mx-auto">
           <p>Unauthorized! You must be logged in.</p> 
            <h2>
                <Link to="/login"> Login</Link>
            </h2>
        </div>
    )
}
export default Auth;