import React, {useState} from 'react'; 
import EditTodo from './forms/EditTodo';

const TodoItem = ({ todo, completeTodo,editTodo, deleteTodo }) => {
    const [editing, setEditing] = useState(false)
    let completedStyle = {
        textDecoration: "line-through",
        color: "black",
        fontStyle: "italic"
    }
    return (
        <div>
            <li style={todo.isCompleted ? completedStyle : null}>
                {todo.name}
                {editing ? <EditTodo todo={todo} editTodo={editTodo}/> : null}
                <br />
                {!todo.isCompleted ? 
                    <div>
                        <button onClick={() => completeTodo(todo._id)}>Complete</button>
                        <button onClick={() => setEditing(!editing)}>Edit</button>
                        <button onClick={() => deleteTodo(todo._id)}>Delete</button>
                    </div> : null
                }
                
            </li>
        </div>
    )
}

export default TodoItem;