import React, {useEffect, useState} from 'react';
import TodoItem from './components/TodoItem';
import AddTodo from './components/forms/AddTodo';

function App() {
  const [todos, setTodos] = useState([])
  useEffect(() => {
    fetch("http://localhost:4000/todos")
      .then(res => res.json())
      .then(data => {
      setTodos(data)
    })
  }, [todos])
  //console.log(todos)
  
  //adding todo
  const addTodo = (todo) => {
   // alert("Add Todo")
   // console.log(todo)
    fetch("http://localhost:4000/todos", {
      method: "POST",
      body: JSON.stringify(todo),
      headers: {
        "Content-type" : "application/json"
      }
    })
      .then(res => res.json())
      .then(data => {
      console.log(data)
    })
  }
  

  //complete todo
  const completeTodo = (id) => {
    //console.log(id)
    fetch(`http://localhost:4000/todos/${id}`, {
      method: "PATCH"
    })
    .then(res => res.json())
    .then(data => console.log(data))
  }

//delete Todo
  const deleteTodo = (id) => {
    let result = window.confirm("Are you sure want to delete this todo?")
    if(result){
    fetch(`http://localhost:4000/todos/${id}`, {
      method: "DELETE"
    })
      .then(res => res.json())
      .then(data => console.log(data))
    }
  }
  

  const editTodo = (newTodo) => {
    //console.log(newTodo)
    fetch(`http://localhost:4000/todos/${newTodo.id}`, {
      method: "PUT",
      body: JSON.stringify(newTodo),
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => res.json())
    .then(data => console.log(data))
  }

  const allTodos = todos.map(todo => <TodoItem
    todo={todo}
    key={todo._id}
    completeTodo={completeTodo}
    deleteTodo={deleteTodo}
    editTodo={editTodo}
  />)
  return (
    <div className="App">
     <AddTodo
      addTodo={addTodo}
      />
      {allTodos}
    </div>
  );
}

export default App;
